FROM golang:1.12 as builder

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY main.go .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:3.11.0

LABEL maintainer="José Hisse"

ENV GIN_MODE release

USER 1000:1000

WORKDIR /app

COPY --from=builder /app/main .

EXPOSE 8080

CMD ["./main"]