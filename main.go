package main

import "github.com/gin-gonic/gin"
import "net/http"

var (
	r *gin.Engine
)

func main() {
	r = gin.Default()
	r.GET("/ping", ping)
	r.Run()
}

func ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "pong",
	})
}
